
chan ch1 = [0] of {byte};
chan ch2 = [0] of {byte};
chan ch3 = [0] of {byte};
chan ch4 = [0] of {byte};

int has_token[4];

proctype Processor(chan ch_l;chan ch_r;int n) {
	do

		:: atomic { ch_l? 1 -> hastoken[n]=1; } atomic { hastoken[n]=0; ch_r! 1 }
		:: atomic { ch_l? 1 -> hastoken[n]=1; } atomic { hastoken[n]=0; ch_l! 1 }

		:: atomic { ch_r? 1 -> hastoken[n]=1; } atomic { hastoken[n]=0; ch_l! 1 }
		:: atomic { ch_r? 1 -> hastoken[n]=1; } atomic { hastoken[n]=0; ch_r! 1 }

	od
}

init {
		ch1! 1;
		//do::
		//	assert((has_token[0] + has_token[1] + has_token[2] + has_token[3]) <= 1)
		//od
		run my_process(ch1,ch2,0);
		run my_process(ch2,ch3,1);
		run my_process(ch3,ch4,2);
		run my_process(ch4,ch1,3)
}

// A
never {
	has_token[0] + has_token[1] 
		+ has_token[2] + has_token[3] > 1; 
}

// B
never {
	do
		::has_token[0] + has_token[1] 
			+ has_token[2] + has_token[3] == 0
	od	
}